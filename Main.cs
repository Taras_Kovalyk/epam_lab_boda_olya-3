using System;

namespace test
{
	public class Program
	{
		public static void Main()
		{
			#region Vector 
			var vector1 = new Vector(3, 4, 5);
			var vector2 = new Vector(3, 4, 5);
			var vector3 = new Vector(5, 7, 8);
			var vector4 = new Vector(1, 7, 8);
			var crossProductVec = vector1.GetCrossProduct(vector3);
			var subtractVec = vector2 - vector3;
			var addVec = vector1 + vector2;

			Console.WriteLine("Task 1. Input data:");
			Console.WriteLine("\tvector1 = {0}", vector1);
			Console.WriteLine("\tvector2 = {0}", vector2);
			Console.WriteLine("\tvector3 = {0}", vector3);
			Console.WriteLine("\tvector4 = {0}", vector4);

			Console.WriteLine("\nUsage demo:\n");
			Console.WriteLine("The length of each vectors:\n\tvector1 = {0:#.##};\n\tvector2 = {1:#.##};\n\tvector3 = {2:#.##};\n\tvector4 = {3:#.##};\n",
			vector1.Length, vector2.Length, vector3.Length, vector4.Length);

			Console.WriteLine("Scalar product: vector1 * vector3 = {0}\n", vector1.GetScalarProduct(vector3));
			Console.WriteLine("Cross product: vector1 x vector3 = {0}\n", crossProductVec);
			Console.WriteLine("Triple product: vector1 * (vector2 x vector3) = {0:#0.##}\n", vector1.GetTripleProduct(vector2, vector3));
			Console.WriteLine("Angle beatween vector1 and vectot2 is: {0:#0.##}\n", vector1.GetAngle(vector2));

			Console.WriteLine("vector2 - vector3 = {0}\n", subtractVec);
			Console.WriteLine("vector1 + vector2 = {0}\n", addVec);

			Console.WriteLine("vector1 == vector2: {0}\n", vector1 == vector2);
			Console.WriteLine("vector1 != vector3: {0}\n", vector1 != vector3);
			Console.WriteLine("vector1 > vector3: {0}\n", vector1 > vector3);
			Console.WriteLine("vector2 < vector4: {0}", vector2 < vector4);
			Console.WriteLine("Press any key to continue");
			Console.ReadKey();
			#endregion
			#region Rectangle
			Console.WriteLine("\nTask 2. Input data:");
			var a = new Point(1, 1);
			var rectangle2 = new Rectangle(new Point(3, 3), 2, 3);
			var rectangle = new Rectangle(new Point(1, 4), 3, 3);
			var resultCombine = rectangle.CombineRectangles(rectangle2);
			var resultSection = rectangle.GetSection(rectangle2);

			Console.WriteLine("First rectangle have vertex: A1={0}, B1={1}, C1={2}, D1={3}\n", rectangle.UpperLeftVert, rectangle.LowerLeftVert, rectangle.LowerRightVert, rectangle.UpperRightVert);

			Console.WriteLine("Second rectangle have vertex: A2={0}, B2={1}, C2={2}, D2={3}\n", rectangle2.UpperLeftVert, rectangle2.LowerLeftVert, rectangle2.LowerRightVert, rectangle2.UpperRightVert);

			Console.WriteLine("Combine first and second rectangles: A3 = {0}, B3 = {1}, C3 = {2}, D3 = {3}\n", resultCombine.UpperLeftVert, resultCombine.LowerLeftVert, resultCombine.LowerRightVert, resultCombine.UpperRightVert);

			Console.WriteLine("Section first and second rectangle: A4={0}, B4={1}, C4={2}, D4={3}\n", resultSection.UpperLeftVert, resultSection.LowerLeftVert, resultSection.LowerRightVert, resultSection.UpperRightVert);

			rectangle.ChangeSizeTo(3, 4);

			Console.WriteLine("Firs rectangle after resizing (width = 3, height = 4): A1={0}, B1={1}, C1={2}, D1={3}\n", rectangle.UpperLeftVert, rectangle.LowerLeftVert, rectangle.LowerRightVert, rectangle.UpperRightVert);

			rectangle.MoveTo(a);

			Console.WriteLine("First rectangle after moving on the vector a=[{8}, {9}]: A=({0},{1}), B=({2}, {3}), C=({4}, {5}), D=({6}, {7})\n", rectangle.UpperLeftVert.X, rectangle.UpperLeftVert.Y, rectangle.LowerLeftVert.X, rectangle.LowerLeftVert.Y, rectangle.LowerRightVert.X, rectangle.LowerRightVert.Y, rectangle.UpperRightVert.X, rectangle.UpperRightVert.Y, a.X, a.Y);
			Console.WriteLine("Press any key to continue");
		    Console.ReadKey();
			#endregion
			#region Array
			Console.WriteLine("\nTask 3. Input data:");
			var array1 = new Array(5, 10);
			var array2 = new Array(5, 10);
			var array3 = new Array(8, -2);
			var array4 = new Array(5, 10);
			var array5 = new Array(5, 10);
			array2.Fill(20);
			array1.Fill(20);
			array3.Fill(30);
			array4.Fill(10);
			array5.Fill(40);
			
			Console.WriteLine("array1 with begin index {0} and length {1}", array1.BeginIndex, array1.Length);
			array1.Print();	
			Console.WriteLine("\narray2 with begin index {0} and length {1}", array2.BeginIndex, array2.Length);
			array2.Print();
			Console.WriteLine("\narray3 with begin index {0} and length {1}", array3.BeginIndex, array3.Length);
			array3.Print();
			Console.WriteLine("\narray4 with begin index {0} and length {1}", array4.BeginIndex, array4.Length);
			array4.Print();
			Console.WriteLine("\narray5 with begin index {0} and length {1}", array5.BeginIndex, array5.Length);
			array5.Print();
			
			Console.WriteLine("\narray1 + array2:");
			var resultAdd = Array.Add(array1, array2);
			resultAdd.Print();
			
			Console.WriteLine("\narray1 - array2:");
			var resultSub = Array.Subtract(array1, array2);
			resultSub.Print();
			
			Console.WriteLine("\narray3 * 2:");
			var resultMult = Array.Multiply(array3, 2);
			resultMult.Print();
			
			Console.WriteLine("\narray1 == array2: {0}\n", array1 == array2);
			Console.WriteLine("\narray1 != array3: {0}\n", array1 == array2);
			Console.WriteLine("\narray1 > array4: {0}\n", array1 > array4);
			Console.WriteLine("\narray1 < array5: {0}\n", array1 == array2);
			Console.WriteLine("Press any key to continue");
			Console.ReadKey();
			#endregion
			#region
			Console.WriteLine("\nTask 4. Input data:");
			var matrix1 = new Matrix(3, 4);
			var matrix2 = new Matrix(4, 4);
			var matrix3 = new Matrix(3, 4);
			var matrix4 = new Matrix(3, 4);
			var beginRow = 1;
			var beginCol = 1;
			var rows = 2;
			var cols = 3;
			
			matrix1.Fill(10);
			matrix2.Fill(20);
			matrix3.Fill(10);
			matrix4.Fill(30);
			
			var resultMultMatrix = matrix1 * matrix2;
			var resultAddMatrix = matrix1 + matrix3;
			var resultSubMatrix = matrix1 - matrix4;
			var transpose = matrix1.GetTransposeMatrix();
			var multAtNum = matrix2.MultiplyAtNumber(3);
			var subMatrix = matrix1.GetSubMatrix(beginRow, beginCol, rows, cols);
			
			Console.WriteLine("matrix1:");
			matrix1.Print();
			Console.WriteLine("matrix2:");
			matrix2.Print();
			Console.WriteLine("matrix3:");
			matrix3.Print();
			Console.WriteLine("matrix4:");
			matrix4.Print();
			
			Console.WriteLine("matrix1 * matrix2:");
			resultMultMatrix.Print();
			Console.WriteLine("matrix1 + matrix3:");
			resultAddMatrix.Print();
			Console.WriteLine("matrix1 - matrix4:");
			resultSubMatrix.Print();
			
			Console.WriteLine("Transpose matrix to matrix1:");
			transpose.Print();
			
			Console.WriteLine("Multiply matrix2 at number 3");
			multAtNum.Print();
			
			Console.WriteLine("Submatrix[{2}, {3}] from matrix1 with begin index ({0}, {1}):", beginRow, beginCol, rows, cols);
			subMatrix.Print();
			
			Console.WriteLine("matrix1 == matrix3: {0}", matrix1 == matrix3);
			Console.WriteLine("matrix1 != matrix4: {0}", matrix1 != matrix4);
			Console.WriteLine("matrix1 < matrix4: {0}", matrix1 < matrix4);
			Console.WriteLine("matrix3 > matrix4: {0}", matrix3 > matrix4);
			Console.WriteLine("Finished. Press any key to exit.");
			Console.ReadKey();
			#endregion
     	}
	}
}