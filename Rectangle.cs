using System;

namespace test
{
	public struct Point
	{
		public double X { get; set; }
		public double Y { get; set; }

		public Point(double x, double y) : this()
		{
			X = x;
			Y = y;
		}

		public override string ToString()
		{
			return string.Format("({0:#0.##}; {1:#0.##})", X, Y);
		}
	}

	class Rectangle
	{
		public Rectangle(Point upperLeftVert, double width, double height)
		{
			UpperLeftVert = upperLeftVert;
			Width = width;
			Height = height;
		}

		public Point UpperLeftVert { get; set; }

		public Point UpperRightVert
		{
			get
			{
				return new Point(UpperLeftVert.X + Width, UpperLeftVert.Y);
			}

		}
		public Point LowerLeftVert
		{
			get
			{
				return new Point(UpperLeftVert.X, UpperLeftVert.Y - Height);
			}

		}
		public Point LowerRightVert
		{
			get
			{
				return new Point(UpperLeftVert.X + Width, UpperLeftVert.Y - Height);
			}
		}
		public double Width { get; set; }

		public double Height { get; set; }

		public void MoveTo(Point vector)
		{
			UpperLeftVert = new Point(UpperLeftVert.X + vector.X, UpperRightVert.Y + vector.Y);
		}

		public void ChangeSizeTo(double width, double height)
		{
			if (width <= 0 || height <= 0)
			{
				throw new Exception("Incorrec size of rectangle");
			}

			Width = width;
			Height = height;
		}

		public Rectangle CombineRectangles(Rectangle other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			double xBegin = Math.Min(this.UpperLeftVert.X, other.UpperLeftVert.X);
			double yBegin = Math.Max(this.UpperLeftVert.Y, other.UpperLeftVert.Y);
			double xEnd = Math.Max(this.UpperLeftVert.X + this.Width, other.UpperLeftVert.X + other.Width);
			double yEnd = Math.Min(this.UpperLeftVert.Y - this.Height, other.UpperLeftVert.Y - other.Height);

			return new Rectangle(new Point(xBegin, yBegin), xEnd - xBegin, yBegin - yEnd);
		}

		public Rectangle GetSection(Rectangle other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			
			double xBegin = Math.Max(this.UpperLeftVert.X, other.UpperLeftVert.X);
			double yBegin = Math.Min(this.UpperLeftVert.Y, other.UpperLeftVert.Y);
			double xEnd = Math.Min(this.UpperLeftVert.X + this.Width, other.UpperLeftVert.X + other.Width);
			double yEnd = Math.Max(this.UpperLeftVert.Y - this.Height, other.UpperLeftVert.Y - other.Height);

			if (xEnd > xBegin && yEnd < yBegin)
			{
				return new Rectangle(new Point(xBegin, yBegin), xEnd - xBegin, yBegin - yEnd);
			}
			return null;
		}
	}
	{
		public static void Main()
		{
			var a = new Point(1, 1);
			var rectangle2 = new Rectangle(new Point(3, 3), 2, 3);
			var rectangle = new Rectangle(new Point(1, 4), 3, 3);
			var resultCombine = rectangle.CombineRectangles(rectangle2);
			var resultSection = rectangle.GetSection(rectangle2);

			Console.WriteLine("First rectangle have vertex: A1={0}, B1={1}, C1={2}, D1={3}\n", rectangle.UpperLeftVert, rectangle.LowerLeftVert, rectangle.LowerRightVert, rectangle.UpperRightVert);

			Console.WriteLine("Second rectangle have vertex: A2={0}, B2={1}, C2={2}, D2={3}\n", rectangle2.UpperLeftVert, rectangle2.LowerLeftVert, rectangle2.LowerRightVert, rectangle2.UpperRightVert);

			Console.WriteLine("Combine first and second rectangles: A3 = {0}, B3 = {1}, C3 = {2}, D3 = {3}\n", resultCombine.UpperLeftVert, resultCombine.LowerLeftVert, resultCombine.LowerRightVert, resultCombine.UpperRightVert);

			Console.WriteLine("Section first and second rectangle: A4={0}, B4={1}, C4={2}, D4={3}\n", resultSection.UpperLeftVert, resultSection.LowerLeftVert, resultSection.LowerRightVert, resultSection.UpperRightVert);


			rectangle.ChangeSizeTo(3, 4);

			Console.WriteLine("Firs rectangle after resizing (width = 3, height = 4): A1={0}, B1={1}, C1={2}, D1={3}\n", rectangle.UpperLeftVert, rectangle.LowerLeftVert, rectangle.LowerRightVert, rectangle.UpperRightVert);

			rectangle.MoveTo(a);

			Console.WriteLine("First rectangle after moving on the vector a=[{8}, {9}]: A=({0},{1}), B=({2}, {3}), C=({4}, {5}), D=({6}, {7})\n", rectangle.UpperLeftVert.X, rectangle.UpperLeftVert.Y, rectangle.LowerLeftVert.X, rectangle.LowerLeftVert.Y, rectangle.LowerRightVert.X, rectangle.LowerRightVert.Y, rectangle.UpperRightVert.X, rectangle.UpperRightVert.Y, a.X, a.Y);
		  
		  Console.ReadKey();
		}
	}*/
}